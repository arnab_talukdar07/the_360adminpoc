export default {

    namespace: true,
  
    state: {
      guards:{},
      addGuards:{},
      editGuards:{},
      //Checkpoint
      guardsCheckpoint:{},
      addGuardsPatrollingCheckpoint:{},
      editGuardsPatrollingCheckpoint:{},
      //Routes
      guardsPatrollingRoutes:{},
      guardsPatrollingRoutesCheckpoints:{},
      guardsPatrollingRoutesNavigate:{
        before:'',
        after:''
      },
      //Logs
      guardsLogs:{},
      guardsLogsNavigate:{
        before:'',
        after:''
      }
     

    },
  
    getters: {
      getGuards(state) {
        return state.guards;
      },
      getAddGuards(state) {
        return state.addGuards;
      },
      getEditGuards(state){
        return state.editGuards;
      },
      //Checkpoint
      getGuardsCheckpoint(state){
        console.log(state.guardsCheckpoint);
        return state.guardsCheckpoint;
      },
      getAddGuardsPatrollingCheckpoint(state){
        return state.addGuardsPatrollingCheckpoint;
      },
      getEditGuardsPatrollingCheckpoint(state){
        return state.editGuardsPatrollingCheckpoint;
      },
      //Routes
      getGuardsPatrollingRoutes(state){
        return state.guardsPatrollingRoutes;
      },
      getGuardsPatrollingRoutesCheckpoints(state){
        return state.guardsPatrollingRoutesCheckpoints;
      },
      getGuardsPatrollingRoutesCheckpointsNavigate(state){
        return state.guardsPatrollingRoutesNavigate;
      },
      //Logs
      getGuardsLogs(state){
        return state.guardsLogs;
      },
      getGuardsLogsNavigate(state){
        return state.guardsLogsNavigate;
      }

    },
  
    actions: {
  
      
      async guards({ commit ,getters},ob) {

        try {
    
          const response =  await this.$axios.get('/guards/list');
          const { status } = response.data;
          
         commit('SET_GUARDS', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          }
        }
        },
      async addGuard({ commit ,dispatch },guard) {
        try {
    
          const response =  await this.$axios.post('/guards/create',guard);
          const { status } = response.data;
          
          console.log(response.data.data);
          
         commit('SET_ADD_GUARDS', response.data);
         this.$swal({
          title: status.message,
          icon:'success',
          showConfirmButton: false,
          timer: 2000,
       });
       dispatch('guards');
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          const { data } = ex.response;
          const { errors, status } = data;
          commit('SET_ADD_GUARDS', { 'message': status.message });
          this.$swal({
            title: status.message,
            icon:'error',
            showConfirmButton: false,
            timer: 2000,
         });
          
          if(status.message=="Validation Error"){
          commit('SET_ADD_GUARDS', { 'message': status.message, 'type': 'error', 'errors': errors });
          console.log(data);
        }
          else{
            commit('SET_ADD_GUARDS', { 'message': status.message, 'type': 'error', 'errors': errors });
            console.log(data);
          }
          }
        }
        },
        async editGuards({ commit ,dispatch },guard) {
          try {
      
            const response =  await this.$axios.post('/guards/edit',guard);
            const { status } = response.data;
            
            console.log(response.data.data);
            
           commit('SET_EDIT_GUARDS', response.data);
           this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
         dispatch('guards');
            
          } catch (ex) {
            if (ex.response) { // When it is an response error
            //   this.$router.push('/login');
            const { data } = ex.response;
            const { errors, status } = data;
            commit('SET_EDIT_GUARDS', { 'message': status.message });
            this.$swal({
              title: status.message,
              icon:'error',
              showConfirmButton: false,
              timer: 2000,
           });
            
            if(status.message=="Validation Error"){
            commit('SET_EDIT_GUARDS', { 'message': status.message, 'type': 'error', 'errors': errors });
            console.log(data);
          }
            else{
              commit('SET_EDIT_GUARDS', { 'message': status.message, 'type': 'error', 'errors': errors });
              console.log(data);
            }
            }
          }
          },

        //checkpoint
        async guardsCheckpoint({ commit ,getters}) {

          try {
      
            const response =  await this.$axios.get('/guard-patrolling/checkpoint/list');
            const { status } = response.data;
            console.log(response.data.data);
           commit('SET_GUARDS_CHECKPOINT', response.data.data);
            
          } catch (ex) {
            if (ex.response) { // When it is an response error
            //   this.$router.push('/login');
            }
          }
          },
          async addGuardsPatrollingCheckpoint({ commit ,dispatch },guard) {
            try {
        
              const response =  await this.$axios.post('/guard-patrolling/checkpoint/create',guard);
              const { status } = response.data;
              
              console.log(response.data.data);
              
             commit('SET_ADD_GUARDS_PATROLLING_CHECKPOINT', response.data);
             this.$swal({
              title: status.message,
              icon:'success',
              showConfirmButton: false,
              timer: 2000,
           });
           dispatch('guardsCheckpoint');
              
            } catch (ex) {
              if (ex.response) { // When it is an response error
              //   this.$router.push('/login');
              const { data } = ex.response;
              const { errors, status } = data;
              commit('SET_ADD_GUARDS_PATROLLING_CHECKPOINT', { 'message': status.message });
              this.$swal({
                title: status.message,
                icon:'error',
                showConfirmButton: false,
                timer: 2000,
             });
              
              if(status.message=="Validation Error"){
              commit('SET_ADD_GUARDS_PATROLLING_CHECKPOINT', { 'message': status.message, 'type': 'error', 'errors': errors });
              console.log(data);
            }
              else{
                commit('SET_ADD_GUARDS_PATROLLING_CHECKPOINT', { 'message': status.message, 'type': 'error', 'errors': errors });
                console.log(data);
              }
              }
            }
            },
            async editGuardsPatrollingCheckpoint({ commit ,dispatch },guard) {
              try {
          
                const response =  await this.$axios.post('/guard-patrolling/checkpoint/edit',guard);
                const { status } = response.data;
                
                console.log(response.data.data);
                
               commit('SET_EDIT_GUARDS_PATROLLING_CHECKPOINT', response.data);
               this.$swal({
                title: status.message,
                icon:'success',
                showConfirmButton: false,
                timer: 2000,
             });
             dispatch('guardsCheckpoint');
                
              } catch (ex) {
                if (ex.response) { // When it is an response error
                //   this.$router.push('/login');
                const { data } = ex.response;
                const { errors, status } = data;
                commit('SET_EDIT_GUARDS_PATROLLING_CHECKPOINT', { 'message': status.message });
                this.$swal({
                  title: status.message,
                  icon:'error',
                  showConfirmButton: false,
                  timer: 2000,
               });
                
                if(status.message=="Validation Error"){
                commit('SET_EDIT_GUARDS_PATROLLING_CHECKPOINT', { 'message': status.message, 'type': 'error', 'errors': errors });
                console.log(data);
              }
                else{
                  commit('SET_EDIT_GUARDS_PATROLLING_CHECKPOINT', { 'message': status.message, 'type': 'error', 'errors': errors });
                  console.log(data);
                }
                }
              }
              },
              //Routes
              async guardsPatrollingRoutes({ commit ,getters}) {

                try {
            
                  const response =  await this.$axios.get('/guard-patrolling/route/list?');
                  const { status } = response.data;
                  console.log(response.data.data);
                 commit('SET_GUARDS_PATROLLING_ROUTES', response.data.data);
                  
                } catch (ex) {
                  if (ex.response) { // When it is an response error
                  //   this.$router.push('/login');
                  }
                }
                },
                async guardsPatrollingRoutesCheckpoints({ commit ,getters}) {

                  try {
              
                    const response =  await this.$axios.get('/guard-patrolling/checkpoints/all');
                    const { status } = response.data;
                    console.log(response.data.data);
                   commit('SET_GUARDS_PATROLLING_ROUTES_CHECKPOINTS', response.data.data);
                    
                  } catch (ex) {
                    if (ex.response) { // When it is an response error
                    //   this.$router.push('/login');
                    }
                  }
                  },
                //Logs
                async guardsLogs({ commit ,getters}) {
                  let filter=getters.getGuardsPatrollingRoutesCheckpointsNavigate;
                  try {
              
                    const response =  await this.$axios.post('/guard-patrolling/patrolling/list?search_term=&before_cursor='+filter.before+'&after_cursor='+filter.after,{});
                    const { status } = response.data;
                    console.log(response.data.data);
                   commit('SET_GUARDS_LOGS', response.data.data);
                    
                  } catch (ex) {
                    if (ex.response) { // When it is an response error
                    //   this.$router.push('/login');
                    }
                  }
                  },

       
    },
  
    mutations: {
      SET_GUARDS(state, value) {
        state.guards = value;
      },
      SET_ADD_GUARDS(state, value){
        state.addGuards=value;
      },
      SET_EDIT_GUARDS(state, value){
        state.editGuards=value;
      },
      //Checkpoint
      SET_GUARDS_CHECKPOINT(state, value){
        console.log(value);
        state.guardsCheckpoint=value;
        console.log(state.guardsCheckpoint);
      },
      SET_ADD_GUARDS_PATROLLING_CHECKPOINT(state, value){
        state.addGuardsPatrollingCheckpoint=value;
      },
      SET_EDIT_GUARDS_PATROLLING_CHECKPOINT(state, value){
        state.editGuardsPatrollingCheckpoint=value;
      },
      //Routes
      SET_GUARDS_PATROLLING_ROUTES(state, value){
        state.guardsPatrollingRoutes=value;
      },
      SET_GUARDS_PATROLLING_ROUTES_CHECKPOINTS(state, value){
        state.guardsPatrollingRoutesCheckpoints=value;
      },
      //Logs
      SET_GUARDS_LOGS(state, value){
        state.guardsLogs=value;
      },
      SET_GUARDS_LOGS_NAVIGATE(state, value){
        state.guardsLogsNavigate=value;
      }
    }
  }