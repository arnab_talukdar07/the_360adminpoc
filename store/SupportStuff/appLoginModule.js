export default {

    namespace: true,
  
    state: {
      appLogin:{},
      addAppLogin:{},
      editAppLogin:{}

    },
  
    getters: {
      getAppLogin(state) {
        return state.appLogin;
      },
      getAddAppLogin(state) {
        return state.addAppLogin;
      },
      getEditAppLogin(state){
        return state.editAppLogin;
      }
    },
  
    actions: {
  
      
      async appLogin({ commit ,getters},ob) {

        try {
    
          const response =  await this.$axios.get('/app-login/list');
          const { status } = response.data;
          
         commit('SET_APP_LOGIN', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          }
        }
        },
      async addAppLogin({ commit ,dispatch },guard) {
        try {
    
          const response =  await this.$axios.post('/appLogin/create',guard);
          const { status } = response.data;
          
          console.log(response.data.data);
          
         commit('SET_ADD_APP_LOGIN', response.data);
         this.$swal({
          title: status.message,
          icon:'success',
          showConfirmButton: false,
          timer: 2000,
       });
       dispatch('appLogin');
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          const { data } = ex.response;
          const { errors, status } = data;
          commit('SET_ADD_APP_LOGIN', { 'message': status.message });
          this.$swal({
            title: status.message,
            icon:'error',
            showConfirmButton: false,
            timer: 2000,
         });
          
          if(status.message=="Validation Error"){
          commit('SET_ADD_APP_LOGIN', { 'message': status.message, 'type': 'error', 'errors': errors });
          console.log(data);
        }
          else{
            commit('SET_ADD_APP_LOGIN', { 'message': status.message, 'type': 'error', 'errors': errors });
            console.log(data);
          }
          }
        }
        },
        async editAppLogin({ commit ,dispatch },guard) {
          try {
      
            const response =  await this.$axios.post('/appLogin/edit',guard);
            const { status } = response.data;
            
            console.log(response.data.data);
            
           commit('SET_EDIT_APP_LOGIN', response.data);
           this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
         dispatch('appLogin');
            
          } catch (ex) {
            if (ex.response) { // When it is an response error
            //   this.$router.push('/login');
            const { data } = ex.response;
            const { errors, status } = data;
            commit('SET_EDIT_APP_LOGIN', { 'message': status.message });
            this.$swal({
              title: status.message,
              icon:'error',
              showConfirmButton: false,
              timer: 2000,
           });
            
            if(status.message=="Validation Error"){
            commit('SET_EDIT_APP_LOGIN', { 'message': status.message, 'type': 'error', 'errors': errors });
            console.log(data);
          }
            else{
              commit('SET_EDIT_APP_LOGIN', { 'message': status.message, 'type': 'error', 'errors': errors });
              console.log(data);
            }
            }
          }
          },
       
    },
  
    mutations: {
      SET_APP_LOGIN(state, value) {
        state.appLogin = value;
      },
      SET_ADD_APP_LOGIN(state, value){
        state.addAppLogin=value;
      },
      SET_EDIT_APP_LOGIN(state, value){
        state.editAppLogin=value;
      }
    }
  }