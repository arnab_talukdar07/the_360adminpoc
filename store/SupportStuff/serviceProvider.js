export default {

    namespace: true,
  
    state: {
      serviceProviders:{}
     

    },
  
    getters: {
      getServiceProviders(state) {
        return state.serviceProviders;
      }
    },
  
    actions: {
  
      
      async serviceProviders({ commit ,getters},ob) {

        try {
    
          const response =  await this.$axios.get('/service-provider/list/new/web');
          const { status } = response.data;
          
         commit('SET_SERVICE_PROVIDER', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          }
        }
        },
     
       
    },
  
    mutations: {
      SET_SERVICE_PROVIDER(state, value) {
        state.serviceProviders = value;
      }
    }
  }