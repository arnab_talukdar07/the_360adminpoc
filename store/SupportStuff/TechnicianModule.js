export default {

    namespace: true,
  
    state: {
      technicians:{},
      addTechnicians:{},
      editTechnicians:{},
      depts:[
        {id: 1, department: "Electricity"},
        {id: 2, department: "Plumber"},
        {id: 3, department: "Carpenter"}
      ]

    },
  
    getters: {
      gettechnicians(state) {
        return state.technicians;
      },
      getDepts(state) {
        return state.depts;
      },
      getAddTechnicians(state) {
        return state.addTechnicians;
      },
      
      getAddTechnicians(state) {
        return state.editTechnicians;
      }
    },
  
    actions: {
  
      
      async technicians({ commit ,getters}) {

        try {
    
          const response =  await this.$axios.get('/technician/list');
          const { status } = response.data;
          
         commit('SET_technicians', response.data.data);
         console.log(response.data.data)
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          }
        }
        },
        async deptList({ commit ,getters}) {

            try {
        
              const response =  await this.$axios.get('/dept/list');
              const { status } = response.data;
              
             commit('SET_DEPT', response.data.data);
             console.log(response.data.data)
              
            } catch (ex) {
              if (ex.response) { // When it is an response error
              //   this.$router.push('/login');
              }
            }
            },
      async addTechnicians({ commit ,dispatch },technician) {
        try {
    
          const response =  await this.$axios.post('/technicians/create',technician);
          const { status } = response.data;
          
          console.log(response.data.data);
          
         commit('SET_ADD_technicians', response.data);
         this.$swal({
          title: status.message,
          icon:'success',
          showConfirmButton: false,
          timer: 2000,
       });
       dispatch('technicians');
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          const { data } = ex.response;
          const { errors, status } = data;
          commit('SET_ADD_technicians', { 'message': status.message });
          this.$swal({
            title: status.message,
            icon:'error',
            showConfirmButton: false,
            timer: 2000,
         });
          
          if(status.message=="Validation Error"){
          commit('SET_ADD_technicians', { 'message': status.message, 'type': 'error', 'errors': errors });
          console.log(data);
        }
          else{
            commit('SET_ADD_technicians', { 'message': status.message, 'type': 'error', 'errors': errors });
            console.log(data);
          }
          }
        }
        },
        async editTechnicians({ commit ,dispatch },guard) {
            try {
        
              const response =  await this.$axios.post('/technicians/edit',guard);
              const { status } = response.data;
              
              console.log(response.data.data);
              
             commit('SET_EDIT_technicians', response.data);
             this.$swal({
              title: status.message,
              icon:'success',
              showConfirmButton: false,
              timer: 2000,
           });
           dispatch('technicians');
              
            } catch (ex) {
              if (ex.response) { // When it is an response error
              //   this.$router.push('/login');
              const { data } = ex.response;
              const { errors, status } = data;
              commit('SET_EDIT_technicians', { 'message': status.message });
              this.$swal({
                title: status.message,
                icon:'error',
                showConfirmButton: false,
                timer: 2000,
             });
              
              if(status.message=="Validation Error"){
              commit('SET_EDIT_technicians', { 'message': status.message, 'type': 'error', 'errors': errors });
              console.log(data);
            }
              else{
                commit('SET_EDIT_technicians', { 'message': status.message, 'type': 'error', 'errors': errors });
                console.log(data);
              }
              }
            }
            },
       
    },
  
    mutations: {
      SET_technicians(state, value) {
        state.technicians = value;
      },
      SET_DEPT(state, value) {
        state.depts = [
            {id: 1, department: "Electricity"},
            {id: 2, department: "Plumber"},
            {id: 3, department: "Carpenter"}
          ];
      },
      SET_ADD_technicians(state, value){
        state.addTechnicians=value;
      },
      SET_EDIT_technicians(state, value){
        state.editTechnicians=value;
      }
    }
  }