export default {

    namespace: true,
  
    state: {
      regularVisitors:{},
      regularCategories:{}
    },
  
    getters: {
      getRegularVisitors(state) {
        return state.regularVisitors;
      },
      getRegularCategories(state) {
        return state.regularCategories;
      }      
    },
  
    actions: {
  
      
      async regularVisitors({ commit ,getters},obj) {
        if(obj==undefined){
          obj={};
        }

        try {
    
          const response =  await this.$axios.post('/visitor/regular/list/admin/v2?before_cursor=&cursor=',obj);
          const { status } = response.data;
          
         commit('SET_REGULAR_VISITORS', response.data.data);
         console.log(response.data.data)
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          }
        }
        },
        async regularCategories({ commit ,getters}) {

            try {
        
              const response =  await this.$axios.get('/visitor/regular/categories');
              const { status } = response.data;
              
             commit('SET_REGULAR_CATEGORIES', response.data.data);
             console.log(response.data.data)
              
            } catch (ex) {
              if (ex.response) { // When it is an response error
              //   this.$router.push('/login');
              }
            }
            }
    },
  
    mutations: {
      SET_REGULAR_VISITORS(state, value) {
        state.regularVisitors = value;
      },
      SET_REGULAR_CATEGORIES(state, value) {
        state.regularCategories = value;
      }
    }
  }