import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

//import Property Modules
import loginModule from './Property/loginModule'
import dashboardModule from './Property/dashboardModule'
import ownersModule from './Property/owner'
import blocksModule from './Property/blocksModule'
import flatsModule from  './Property/flatSModule'
import TenantsModule from  './Property/tenantListModule'
import VehiclesModule from './Property/vehiclesModule'
import ParkingSpaceModule from './Property/parkingSpaceModule'

//import SupportStuff Modules
import guardsModule from './SupportStuff/guardsModule';
import appLoginModule from './SupportStuff/appLoginModule';
import TechnicianModule from './SupportStuff/TechnicianModule';
import visitorModule from './SupportStuff/visitorModule';
import serviceProvider from './SupportStuff/serviceProvider';

export default()=> {
  return new Vuex.Store({
    state:{
      error:{}
    },
    mutations:{
      SET_ERROR(state, value){
        state.error=value;
      }
    },
    modules:{
      //Property
      loginModule,
      dashboardModule,
      ownersModule,
      blocksModule,
      flatsModule,
      TenantsModule,
      VehiclesModule,
      ParkingSpaceModule,

      //SupportStuff
      guardsModule,
      appLoginModule,
      TechnicianModule,
      visitorModule,
      serviceProvider
    }
  })
}