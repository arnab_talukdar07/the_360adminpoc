export default {

    namespace: true,
  
    state: {
       flatOwners: {
            cursors:{
                after: "WzMyNDBd",
                before: "WzE4ODRd",
                hasNext: false,
                hasPrevious: false
            },
            found:false,
            users:[]
        },
        requested_users:{
            cursors:{
                after: "WzMyNDBd",
                before: "WzE4ODRd",
                hasNext: false,
                hasPrevious: false
            },
            found:false,
            users:[]
        },
        all_users:{
            cursors:{
                after: "WzMyNDBd",
                before: "WzE4ODRd",
                hasNext: false,
                hasPrevious: false
            },
            found:false,
            users:[]
        },
        blocks:[],
        units:[],
        curretFlatOwner:{},
        createUserErrors:{
          errors:{},
          status:{}
        },
        editUserErrors:{
          errors:{},
          status:{}
        },
        flatwnersObj:{
					search_params:'',
					block_id:'',
					unit_id:''
				},
				requestedUsersObj:{
					search_params:''
				},
				allUsersObj:{
					search_params:''
				},
    },
  
    getters: {
      getFlatOwners(state) {
        return state.flatOwners;
      },
      getRequestedUsers(state) {
        return state.requested_users;
      },
      getAllUsers(state) {
        return state.all_users;
      },
      getBlocks(state) {
        return state.blocks;
      },
      getUnits(state) {
        return state.units;
      },
      getCurrentFlatOwner(state){
        return state.curretFlatOwner;
      },
      getCreateUserErrors(state){
        console.log(state.createUserErrors)
        return state.createUserErrors;
      },
      getEditUserErrors(state){
        console.log(state.editUserErrors)
        return state.editUserErrors;
      },
      getFlatOwnersObj(state){
        return state.flatwnersObj;
      },
      getRequestedUsersObj(state){
        return state.requestedUsersObj;
      },
      getAllUsersObj(state){
        return state.allUsersObj;
      }

    },
  
    actions: {
  
      async flatOwners({ commit ,getters}) {
        let filters=getters.getFlatOwnersObj;

      try {
  
        const response =  await this.$axios.get('/owner/list?before_cursor=&cursor=&search_param='+filters.search_params+'&is_approved=APPROVED&unit_id='+filters.unit_id+'&block_id='+filters.block_id);
        const { status } = response.data;
        
       commit('SET_FLAT_OWNERS', response.data.data);
        
      } catch (ex) {
        if (ex.response) { // When it is an response error
          this.$router.push('/login');
        }
      }
      },
      async flatOwnersNext({ commit ,getters}) {
        let filters=getters.getFlatOwnersObj;
        let object=getters.getFlatOwners;

      try {
        console.log("next")
  
        const response =  await this.$axios.get('/owner/list?before_cursor=&cursor='+object.cursors.after+'&search_param='+filters.search_params+'&is_approved=APPROVED&unit_id='+filters.unit_id+'&block_id='+filters.block_id);
        const { status } = response.data;
        
       commit('SET_FLAT_OWNERS', response.data.data);
        
      } catch (ex) {
        if (ex.response) { // When it is an response error
          this.$router.push('/login');
        }
      }
      },
      async flatOwnersPrev({ commit ,getters}) {
        let filters=getters.getFlatOwnersObj;
        let object=getters.getFlatOwners;

      try {
  
        const response =  await this.$axios.get('/owner/list?before_cursor='+object.cursors.before+'&cursor=&search_param='+filters.search_params+'&is_approved=APPROVED&unit_id='+filters.unit_id+'&block_id='+filters.block_id);
        const { status } = response.data;
        
       commit('SET_FLAT_OWNERS', response.data.data);
        
      } catch (ex) {
        if (ex.response) { // When it is an response error
          this.$router.push('/login');
        }
      }
      },
      async requestedUsers({ commit, getters }) {
        let filter=getters.getRequestedUsersObj;
        
        try {
          const response =  await this.$axios.get('/owner/list?before_cursor=&cursor=&is_approved=WITH_PENDING&search_param='+filter.search_params);
          const { status } = response.data;
         
         commit('SET_REQUESTED_USERS', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },
      async requestedUsersNext({ commit, getters }) {
        let filter=getters.getRequestedUsersObj;
        let object=getters.getRequestedUsers;
        try {
          const response =  await this.$axios.get('/owner/list?before_cursor=&cursor='+object.cursors.after+'&is_approved=WITH_PENDING&search_param='+filter.search_params);
          const { status } = response.data;
         
         commit('SET_REQUESTED_USERS', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },
      async requestedUsersNext({ commit, getters }) {
        let filter=getters.getRequestedUsersObj;
        let object=getters.getRequestedUsers;
        try {
          const response =  await this.$axios.get('/owner/list?before_cursor='+object.cursors.before+'&cursor=&is_approved=WITH_PENDING&search_param='+filter.search_params);
          const { status } = response.data;
         
         commit('SET_REQUESTED_USERS', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },
      async allUsers({ commit , getters }) {
        let filter=getters.getAllUsersObj;
        try {
          const response =   await this.$axios.get('/unassigned/user/list?before_cursor=&cursor=&search_param='+filter.search_params);
          const { status } = response.data;
          
         commit('SET_ALL_USERS', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },
      async allUsersNext({ commit , getters }) {
        let filter=getters.getAllUsersObj;
        let object=getters.getAllUsers;
        try {
          const response =   await this.$axios.get('/unassigned/user/list?before_cursor=&cursor='+object.cursors.after+'&search_param='+filter.search_params);
          const { status } = response.data;
          
         commit('SET_ALL_USERS', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },
      async allUsersPrev({ commit , getters }) {
        let filter=getters.getAllUsersObj;
        let object=getters.getAllUsers;
        try {
          const response =   await this.$axios.get('/unassigned/user/list?before_cursor='+object.cursors.before+'&cursor=&search_param='+filter.search_params);
          const { status } = response.data;
          
         commit('SET_ALL_USERS', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },


      async blocks({ commit }) {
        try {
          const response =   await this.$axios.get('/block/list/admin');
          const { status } = response.data;
          
         commit('SET_BLOCKS', response.data.data.blocks);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },
      async units({ commit }) {
        try {
          const response =   await this.$axios.get('/user/units/dropdown');
          const { status } = response.data;
          
         commit('SET_UNITS', response.data.data.unit_list);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },
      async editUser({ commit },user) {
        try {
  
          const response = await this.$axios.post('/user/edit',user);
         
          const { status } = response.data;
          this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
          commit('SET_EDITUSER_ERRORS', {
            'message': status.message,
            'type': 'success',
            'errors': {}
          });
        } catch (ex) {
          if (ex.response) { // When it is an response error
            const { data } = ex.response;
            const { errors, status } = data;
            console.log(data);
            if(status.message=="Validation Error"){
            commit('SET_EDITUSER_ERRORS', { 'message': status.message, 'type': 'error', errors });}
            else{
              commit('SET_EDITUSER_ERRORS', { 'message': status.message, 'type': 'error', 'errors': {} })
            }
          }
        }
      },
      async createUser({ commit },user) {
        try {
  
          const response = await this.$axios.post('/user/create',user);
         
          const { status } = response.data;
  
          this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
         
          commit('SET_CREATEUSER_ERRORS', {
            'message': status.message,
            'type': 'success',
            'errors': {}
          });
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            const { data } = ex.response;
            const { errors, status } = data;
            console.log(data);
            if(status.message=="Validation Error"){
            commit('SET_CREATEUSER_ERRORS', { 'message': status.message, 'type': 'error', errors });}
            else{
              commit('SET_CREATEUSER_ERRORS', { 'message': status.message, 'type': 'error', 'errors': {} })
            }
          }
        }
      },
      async addUnit({ commit },unit) {
        try{
  
        const response = await this.$axios.post('/unit/occupancy/add',unit)
                  
         console.log(response)
         const { status } =response.status;
         this.$swal({
          title: 'Successfully Updated!',
          icon:'success',
          showConfirmButton: false,
          timer: 2000,
          });
        }
        catch{

        }
      },
      async addUnit({ commit },unit) {
        try{
  
        const response = await this.$axios.post('/unit/occupancy/add',unit)
                  
         console.log(response)
         const { status } =response.status;
         this.$swal({
          title: 'Successfully Updated!',
          icon:'success',
          showConfirmButton: false,
          timer: 2000,
          });
        }
        catch{
          
        }
      },
      async deleteUnit({ commit },unit) {
        
        try{
        const response=await this.$axios.post('/unit/occupancy/remove',unit)
          console.log("not done")
          console.clear();
          console.log(response.data.status);
          const { status } =response.data.status;
          this.$swal({
            title: response.data.status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
        });}
     catch{

     }
      
    },
      async dltUser({ commit },user) {
        try {
  
          const response = await this.$axios.post('/user/delete',user);
          this.refresh();
          const { status } = response.data;
  
          this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
         
          
        } catch (ex) {
         
        }
      },
      
      setCurrentFlatOwner({ commit },user){
        commit('SET_CURR_FLAT_OWNER', user);
      }
    },
  
    mutations: {
      SET_FLAT_OWNERS(state, value) {
        state.flatOwners.cursors = value.cursors;
        state.flatOwners.users = value.users; 
      },
      SET_REQUESTED_USERS(state, value) {
        state.requested_users.cursors = value.cursors;
        state.requested_users.users = value.users; 
      },
      SET_ALL_USERS(state, value) {
        state.all_users.cursors = value.cursors;
        state.all_users.users = value.unassigned_users; 
        state.all_users.found=true;
      },
      SET_BLOCKS(state, value) {
        state.blocks = value;
      },
      SET_UNITS(state, value) {
        state.units = value;
      },
      SET_CURR_FLAT_OWNER(state, value) {

        state.curretFlatOwner=value;
      },
      SET_CREATEUSER_ERRORS(state, value){
        state.createUserErrors=value;
      },
      SET_EDITUSER_ERRORS(state, value){
        state.createUserErrors=value;
      },
      SET_FLAT_OWNERS_OBJ(state, value){
        state.flatwnersObj=value;
      },
      SET_REQUESTED_USERS_OBJ(state, value){
        state.requestedUsersObj=value;
      },
      SET_ALL_USERS_OBJ(state, value){
        state.allUsersObj=value;
      }

    }
  }