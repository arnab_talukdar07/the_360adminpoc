export default {

    namespace: true,
  
    state: {
       tenantList: {
        },
        allTenantApproval:{},
        requestTenant:{},
        tenantObj:{
          before:'',
          after:'',
        },
        allObj:{
          before:'',
          after:''
        },
        requestObj:{
          before:'',
          after:''
        }
       ,
       addTenantUser:{
         user:{}
       },
       addTenantError:{}
    },
  
    getters: {
      getTenantList(state) {
        return state.tenantList;
      },
      getAllTenantApproval(state) {
        return state.allTenantApproval;
      },
      getRequestTenant(state) {
        return state.requestTenant;
      },
      getTenantObj(state){
        return state.tenantObj;
      },
      getAllObj(state){
        return state.allObj;
      },
      getRequestObj(state){
        return state.requestObj;
      },
      getTenantUser(state){
        return state.addTenantUser;
      },
      getTenantError(state){
        return state.addTenantError
      }

    },
  
    actions: {
  
      async tenantList({ commit ,getters}) {
        let filter=getters.getTenantObj;
      try {
  
        const response =  await this.$axios.get('/tenant/searchable-list/by-property?before_cursor='+filter.before+'&cursor='+filter.after+'&search_key_word=');
        const { status } = response.data;
        
        console.log( response.data.data.tenants);
       commit('SET_TENANT_LIST', response.data.data.tenants);
        
      } catch (ex) {
        if (ex.response) { // When it is an response error
          this.$router.push('/login');
        }
      }
      },
      async allTenantApproval({ commit ,getters}) {
        
        let filter=getters.getAllObj;
        try {
    
          const response =  await this.$axios.get('tenant/approval-request/listing?approval_status=APPROVED&search_param=');
          const { status } = response.data;
          
          console.log( response.data.data);
         commit('SET_ALL_TENANT_APPROVAL', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            // this.$router.push('/login');
          }
        }
        },
        async requestTenant({ commit ,getters}) {
          
        let filter=getters.getRequestObj;
          try {
      
            const response =  await this.$axios.get('/tenant/approval-request/listing?approval_status=REQUESTED');
            const { status } = response.data;
            
            console.log( response.data.data);
           commit('SET_REQUEST_TENANT', response.data.data);
            
          } catch (ex) {
            if (ex.response) { // When it is an response error
              // this.$router.push('/login');
            }
          }
          },
          async findByMobile({ commit ,dispatch },phone) {
            try {
        
              const response =  await this.$axios.post('/tenant/find-by-mobile',phone);
              const { status } = response.data;
              
              console.log(response.data.data);
              
             commit('SET_TENANT_USER', response.data.data);
             this.$swal({
              title: status.message,
              icon:'success',
              showConfirmButton: false,
              timer: 2000,
           });
              
            } catch (ex) {
              if (ex.response) { // When it is an response error
              //   this.$router.push('/login');
              const { data } = ex.response;
              const { errors, status } = data;
              commit('SET_TENANT_USER', { 'message': status.message });
              this.$swal({
                title: status.message,
                icon:'error',
                showConfirmButton: false,
                timer: 2000,
             });
              
              if(status.message=="Validation Error"){
              commit('SET_TENANT_USER', { 'message': status.message, 'type': 'error', 'errors': errors });
              console.log(data);
            }
              else{
                commit('SET_TENANT_USER', { 'message': status.message, 'type': 'error', 'errors': errors });
                console.log(data);
              }
              }
            }
            },
          async addTenant({ commit ,dispatch },guard) {
            try {
        
              const response =  await this.$axios.post('/appLogin/create',guard);
              const { status } = response.data;
              
              console.log(response.data.data);
              
             commit('SET_TENANT_ERROR', response.data);
             this.$swal({
              title: status.message,
              icon:'success',
              showConfirmButton: false,
              timer: 2000,
           });
           dispatch('tenantList');
           dispatch('requestTenant');
           dispatch('allTenantApproval');
           
              
            } catch (ex) {
              if (ex.response) { // When it is an response error
              //   this.$router.push('/login');
              const { data } = ex.response;
              const { errors, status } = data;
              commit('SET_TENANT_ERROR', { 'message': status.message });
              this.$swal({
                title: status.message,
                icon:'error',
                showConfirmButton: false,
                timer: 2000,
             });
              
              if(status.message=="Validation Error"){
              commit('SET_TENANT_ERROR', { 'message': status.message, 'type': 'error', 'errors': errors });
              console.log(data);
            }
              else{
                commit('SET_TENANT_ERROR', { 'message': status.message, 'type': 'error', 'errors': errors });
                console.log(data);
              }
              }
            }
            },
            async delTenant({ commit ,dispatch },guard) {
              try {
          
                const response =  await this.$axios.post('/tenant/remove/documents',guard);
                const { status } = response.data;
                
                console.log(response.data.data);
                
               this.$swal({
                title: status.message,
                icon:'success',
                showConfirmButton: false,
                timer: 2000,
             });
             dispatch('tenantList');
             dispatch('requestTenant');
             dispatch('allTenantApproval');
             
                
              } catch (ex) {
                if (ex.response) { // When it is an response error
                }
              }
              },
    },
  
    mutations: {
        SET_TENANT_LIST(state, value) {
        state.tenantList = value;
      },
      SET_ALL_TENANT_APPROVAL(state, value) {
        state.allTenantApproval.cursors = value.cursors;
        state.allTenantApproval.results = value.approval_request;
        
      },
      SET_REQUEST_TENANT(state, value) {
        state.requestTenant.cursors = value.cursors;
        state.requestTenant.results = value.approval_request;
        
      },
      SET_TENANT_OBJ(state, value) {
        state.tenantObj.before = value.before;
        state.tenantObj.after = value.after;
        
      },
      SET_TENANT_USER(state, value){
        state.addTenantUser=value;
      },
      SET_TENANT_ERROR(state, value){
        state.addTenantError=value;
      }

    }
  }