// import uuid from "uuid";

export default {

    namespace: true,
  
    state: {
      loginResponse: {
        'message': '',
        'type': '',
        'errors': {
          'email': '',
          'password': ''
        }
        
      },
  
      logoutResponse: {
        'message': '',
        'type': ''
      }
    },
  
    getters: {
      getLoginResponse(state) {
        return state.loginResponse;
      },
  
      getLogoutResponse(state) {
        return state.logoutResponse;
      }
    },
  
    actions: {
  
      async loginUser({ commit }, loginModel) {
        try {
  
          const response = await this.$auth.loginWith('local', { data: loginModel });
  
          const { status } = response.data;
          this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
          localStorage.setItem('device_id', uuid.v1());
            print(uuid.v1());
  
          // Create a new device_id for the browser
          const existingId = localStorage.getItem('device_id');
          if(!existingId){
            localStorage.setItem('device_id', uuid.v1());
            print(uuid.v1());
          }
  
          commit('SET_LOGIN_RESPONSE', {
            'message': status.message,
            'type': 'success',
            'errors': {}
          });
          this.$router.push('/dashboard');
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            const { data } = ex.response;
            const { errors, status } = data;
            if(status.message=="Validation Error"){
            commit('SET_LOGIN_RESPONSE', { 'message': status.message, 'type': 'error', errors });}
            else{
              commit('SET_LOGIN_RESPONSE', { 'message': status.message, 'type': 'error', 'errors': {} })
            }
          }
        }
      },
  
      async logoutUser({ commit }) {
        const device_id = localStorage.getItem('device_id');
        await this.$auth.logout({ data: { device_id: device_id } }).then(()=>{
          this.$router.push('/login');
        });
        this.$axios.setHeader(null);
        commit('SET_LOGOUT_RESPONSE', {
          'message': 'Logged out successfully',
          'type': 'success'
        });
      }
    },
  
    mutations: {
      SET_LOGIN_RESPONSE(state, value) {
        state.loginResponse = value;
      },
  
      SET_LOGOUT_RESPONSE(state, value) {
        state.logoutResponse = value;
      }
    }
  };