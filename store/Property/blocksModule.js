export default {

    namespace: true,
  
    state: {
      blocks:[],
      adminData:{},
      createBlock:{},
      editBlock:{}


    },
  
    getters: {
      getBlocksW(state) {
        return state.blocks;
      },
      getAdminData(state) {
          console.log(state.adminData)
        return state.adminData;
      },
      getCreateBlock(state){
        console.log(state.createBlock);
        return state.createBlock;
      },
      getEditBlock(state){
        console.log(state.editBlock);
        return state.editBlock;
      }
    },
  
    actions: {
  
      async blocksW({ commit ,getters}) {
        let filters=getters.getFlatOwnersObj;

      try {
  
        const response =  await this.$axios.get('/block/list');
        const { status } = response.data;
        
       commit('SET_BLOCKS', response.data.data.blocks);
        
      } catch (ex) {
        if (ex.response) { // When it is an response error
        //   this.$router.push('/login');
        }
      }
      },
      async adminData({ commit ,getters}) {

      try {
  
        const response =  await this.$axios.get('/block/list/admin');
        const { status } = response.data;
        console.log(response.data.data);
       commit('SET_ADMIN_DATA', response.data.data);
        
      } catch (ex) {
        if (ex.response) { // When it is an response error
        //   this.$router.push('/login');
       
        }
      }
      },
      async createBlock({ commit ,dispatch },block) {
      try {
  
        const response =  await this.$axios.post('/block/create',block);
        const { status } = response.data;
        
        console.log(response.data.data);
        
       commit('SET_CREATE_BLOCK', response.data.data);
       dispatch('blocksW');
       dispatch('adminData');
       this.$swal({
        title: status.message,
        icon:'success',
        showConfirmButton: false,
        timer: 2000,
     });
        
      } catch (ex) {
        if (ex.response) { // When it is an response error
        //   this.$router.push('/login');
        const { data } = ex.response;
        const { errors, status } = data;
        commit('SET_CREATE_BLOCK', { 'message': status.message });
        
        if(status.message=="Validation Error"){
        commit('SET_CREATE_BLOCK', { 'message': status.message, 'type': 'error', 'errors': errors });
        console.log(data);
      }
        else{
          commit('SET_CREATE_BLOCK', { 'message': status.message, 'type': 'error', 'errors': errors });
          console.log(data);
        }
        }
      }
      },
      async editBlock({ commit ,dispatch },block) {
        try {
    
          const response =  await this.$axios.post('/block/edit',block);
          const { status } = response.data;
          
          console.log(response.data.data);
          
         commit('SET_EDIT_BLOCK', response.data.data);
         dispatch('blocksW');
         dispatch('adminData');
         this.$swal({
          title: status.message,
          icon:'success',
          showConfirmButton: false,
          timer: 2000,
       });
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          const { data } = ex.response;
          const { errors, status } = data;
          commit('SET_EDIT_BLOCK', { 'message': status.message });
          
          if(status.message=="Validation Error"){
          commit('SET_EDIT_BLOCK', { 'message': status.message, 'type': 'error', 'errors': errors });
          console.log(data);
        }
          else{
            commit('SET_EDIT_BLOCK', { 'message': status.message, 'type': 'error', 'errors': errors });
            console.log(data);
          }
          }
        }
        }
     
    },
  
    mutations: {
      SET_BLOCKS(state, value) {
        state.blocks = value;
      },
      SET_ADMIN_DATA(state, value) {
        state.adminData=value;
      },
      SET_CREATE_BLOCK(state,value){
        state.createBlock=value;
      },
      SET_EDIT_BLOCK(state,value){
        state.editBlock=value;
      }
    }
  }