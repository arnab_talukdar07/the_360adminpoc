export default {

    namespace: true,
  
    state: {
      unit_listFlat:[],
      filters:{
        before:'',
        after:''
      },
      srchParams:{
        txt:''
      },
      createUnit:{},
      flatUsersList:{}


    },
  
    getters: {
      getFlatUnitList(state) {
        return state.unit_listFlat;
      },
      getFlatUsersList(state) {
        return state.flatUsersList;
      },
      getFilters(state){
        return state.filters;
      },
      getSearch(state){
        return state.srchParams;
      },
      getCreateUnit(state){
        console.log(state.createUnit);
        return state.createUnit;
      }
    },
  
    actions: {
  
      async flatUnitList({ commit, getters }) {
        let filter=getters.getFilters;
        let search=getters.getSearch;
      try {
  
        const response =  await this.$axios.get('/unit/list/admin/paginate?before_cursor='+filter.before+'&cursor='+filter.after+'&search_key_word='+search.txt);
        const { status } = response.data;
        
       commit('SET_UNIT_LIST_FLAT', response.data.data);
        
      } catch (ex) {
        if (ex.response) { // When it is an response error
        //   this.$router.push('/login');
        }
      }
      },
      async flatUsersList({ commit, getters }) {
      try {
  
        const response =  await this.$axios.get('/user/list');
        const { status } = response.data;
        
       commit('SET_USER_LIST_FLAT', response.data.data);
        
      } catch (ex) {
        if (ex.response) { // When it is an response error
        //   this.$router.push('/login');
        }
      }
      },
      async createUnit({ commit ,dispatch },unit) {
        try {
    
          const response =  await this.$axios.post('/unit/create',unit);
          const { status } = response.data;
          
          console.log(response.data.data);
          
         commit('SET_CREATE_UNIt', response.data.data);
         dispatch('flatUnitList');
         this.$swal({
          title: status.message,
          icon:'success',
          showConfirmButton: false,
          timer: 2000,
       });
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');

          const { data } = ex.response;
          const { errors, status } = data;
          this.$swal({
            title: status.message,
            icon:'error',
            showConfirmButton: false,
            timer: 2000,
         });
          commit('SET_CREATE_UNIT', { 'message': status.message });
          
          if(status.message=="Validation Error"){
          commit('SET_CREATE_UNIT', { 'message': status.message, 'type': 'error', 'errors': errors });
          console.log(data);
        }
          else{
            commit('SET_CREATE_UNIT', { 'message': status.message, 'type': 'error', 'errors': errors });
            console.log(data);
          }
          }
        }
        }
     
    },
  
    mutations: {
      SET_UNIT_LIST_FLAT(state, value) {
        state.unit_listFlat = value;
      },
      SET_USER_LIST_FLAT(state, value) {
        state.flatUsersList = value;
      },
      SET_SRCH_PARAMS(state,value){
        state.filters=value;
      },
      SET_CREATE_UNIT(state,value){
        state.createUnit=value;
      },
      SET_SEARCH(state,value){
        state.srchParams=value;
      }
    }
  }