export default {

    namespace: true,
  
    state: {
        dashboard_counts: {
            "unitCount": 0,
            "unitWithFamilyMember": 0,
            "unitWithTenant": 0,
            "serviceProvidersCount": 0,
            "guardCount": 0,
            "regularVisitorCount": 0,
            "complaintCount": 0,
            "committee_member_count": 0,
            "residents": 0,
            "register_count": 0,
            "non_register_count": 0,
            "downloadedButNotRegistered": 0
        },
        firstStats: {
            society: {
              "flats": 0,
              "residents": 0,
              "tenants": 0,
              "occupied": 0,
              "parking": 0,
              "vehicles": 0
            },
            security: {
              "regular_visitors": 0,
              "incidents": 0,
              "alarm_triggers": 0,
              "guard_sleep": 0,
              "senior_citizen_check": 0
            }
        },
        defaulter:{
            "lt10k": 0,
            "tenkTo50k": 0,
            "fiftyTo1lac": 0,
            "oneLacToTwo": 0,
            "twoLacTofive": 0,
            "gt5lac": 0,
            "ltk10": 0
        },
        assets:{
            "active_asset": 0,
            "under_service_asset": 0,
            "expired_asset": 0
        },
        openTickets: {
            "complains": [],
            "cursors": {
              "currentPage": 1,
              "totalPages": 0,
              "nextPage": 2,
              "previousPage": null,
              "hasNext": true,
              "hasPrevious": false
            }
        },
        amenity:[]

    },
  
    getters: {
      getDashboardCounts(state) {
        return state.dashboard_counts;
      },
      getFirstStats(state) {
        return state.firstStats;
      },
      getDefaulters(state) {
        return state.defaulter;
      },
      getAssets(state) {
        return state.assets;
      },
      getOpenTickets(state){
          return state.openTickets;
      },
      getAmenity(state){
        return state.amenity;
    }
    },
  
    actions: {
  
      async dashboard_counts({ commit }) {
        try {
  
          const response = await this.$axios.get('/admin/dashboard');
  
          const { status } = response.data;
          this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
         commit('SET_DASHBOARD_COUNTS', response.data.data.dashboard_counts);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },
      async first_stats({ commit }) {
        try {
  
          const response = await this.$axios.get('/admin/dashboard/society');
  
          const { status } = response.data;
          this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
         commit('SET_FIRST_STATS', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },
      async defaulters({ commit }) {
        try {
  
          const response =await this.$axios.get('/admin/dashboard/defaulter_count');
  
          const { status } = response.data;
          this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
         commit('SET_DEFAULTERS', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },
      async assets({ commit }) {
        try {
  
          const response = await this.$axios.get('/admin/dashboard/asset_count');
  
          const { status } = response.data;
          this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
         commit('SET_ASSETS', response.data.data.asset_count);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      },
      async openTickets({ commit },page) {
        try {
  
          const response =  await this.$axios.get('/complaints/admin/paginate?page='+page+'&issue_status=OPENED&called_from=DASHBOARD');
          const { status } = response.data;
          this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
         commit('SET_OPENTICKETS', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
        
      },
      async amenity({ commit }) {
        try {
  
          const response =  await this.$axios.get('/facility/dashboard-statistics/web');
          const { status } = response.data;
          this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
         commit('SET_AMENITY', response.data.data.facility_statistics);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
            this.$router.push('/login');
          }
        }
      }
    },
  
    mutations: {
      SET_DASHBOARD_COUNTS(state, value) {
        state.dashboard_counts = value;
      },
      SET_FIRST_STATS(state, value) {
        state.firstStats = value;
      },
      SET_DEFAULTERS(state, value) {
        state.defaulter = value;
      },
      SET_ASSETS(state, value) {
        state.assets = value;
      },
      SET_OPENTICKETS(state, value) {
        state.openTickets = value;
      },
      SET_AMENITY(state, value){
          state.amenity=value;
      }
    }
  }