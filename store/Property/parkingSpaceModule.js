export default {

    namespace: true,
  
    state: {
      parking:{},
      addParking:{},
      unitsParking:[],
      parkingDrop:[],
      parkingUsersDrop:[]


    },
  
    getters: {
      getParking(state) {
        return state.parking;
      },
      getAddParking(state) {
        return state.addParking;
      },
      getUnitsParking(state) {
        return state.unitsParking;
      },
      getParkingDrop(state) {
        return state.parkingDrop;
      },
      getParkingUsersDrop(state) {
        return state.parkingDrop;
      }
    },
  
    actions: {
  
      async ParkingUnits({ commit ,getters}) {

      try {
  
        const response =  await this.$axios.get('/unit/list');
        const { status } = response.data;
        
       commit('SET_UNIT_PARKING', response.data.data.unit_list);
        
      } catch (ex) {
        if (ex.response) { // When it is an response error
        //   this.$router.push('/login');
        }
      }
      },
      async parkingDrop({ commit ,getters}) {

        try {
    
          const response =  await this.$axios.get('/parking/dropdown');
          const { status } = response.data;
          
         commit('SET_PARKING_DROP', response.data.data.parkingList);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          }
        }
        },
        async parkingUserDrop({ commit ,getters}) {

            try {
        
              const response =  await this.$axios.get('/v1/user/list/for/dropdown');
              const { status } = response.data;
              
             commit('SET_PARKING_USERS_DROP', response.data.data.drop_down_users);
              
            } catch (ex) {
              if (ex.response) { // When it is an response error
              //   this.$router.push('/login');
              }
            }
            },
      async parking({ commit ,getters},ob) {

        try {
    
          const response =  await this.$axios.get('/parking/list?before_cursor=&cursor='+ob.txt);
          const { status } = response.data;
          
         commit('SET_PARKING', response.data.data);
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          }
        }
        },
      async addParking({ commit ,dispatch },park) {
        try {
    
          const response =  await this.$axios.post('/parking/add',park);
          const { status } = response.data;
          
          console.log(response.data.data);
          
         commit('SET_ADD_PARKING', response.data.data);
         this.$swal({
          title: status.message,
          icon:'success',
          showConfirmButton: false,
          timer: 2000,
       });
       dispatch('parking');
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          const { data } = ex.response;
          const { errors, status } = data;
          commit('SET_ADD_PARKING', { 'message': status.message });
          this.$swal({
            title: status.message,
            icon:'error',
            showConfirmButton: false,
            timer: 2000,
         });
          
          if(status.message=="Validation Error"){
          commit('SET_ADD_PARKING', { 'message': status.message, 'type': 'error', 'errors': errors });
          console.log(data);
        }
          else{
            commit('SET_ADD_PARKING', { 'message': status.message, 'type': 'error', 'errors': errors });
            console.log(data);
          }
          }
        }
        },
       
    },
  
    mutations: {
      SET_PARKING(state, value) {
        state.parking = value;
      },
      SET_ADD_PARKING(state, value){
        state.addParking=value;
      },
      SET_UNIT_PARKING(state, value){
        state.unitsParking=value;
      },
      SET_PARKING_DROP(state, value){
        state.parkingDrop=value;
      },
      SET_PARKING_USERS_DROP(state, value){
        state.parkingUserDrop=value;
      }
    }
  }