export default {

    namespace: true,
  
    state: {
      vehicles:{},
      addVehicle:{},
      editVehicle:{}


    },
  
    getters: {
      getVehicles(state) {
        return state.vehicles;
      },
      getAddVehicles(state) {
        return state.addVehicle;
      },
      getEditVehicles(state) {
        return state.editVehicle;
      }
    },
  
    actions: {
  
      async vehicles({ commit ,getters}) {

      try {
  
        const response =  await this.$axios.get('/vehicle/list');
        const { status } = response.data;
        
       commit('SET_VEHICLES', response.data.data);
        
      } catch (ex) {
        if (ex.response) { // When it is an response error
        //   this.$router.push('/login');
        }
      }
      },
      async addVehicle({ commit ,dispatch },vehicle) {
        try {
    
          const response =  await this.$axios.post('/vehicle/add',vehicle);
          const { status } = response.data;
          
          console.log(response.data.data);
          
         commit('SET_ADD_VEHICLE', response.data.data);
         dispatch('blocksW');
         dispatch('adminData');
         this.$swal({
          title: status.message,
          icon:'success',
          showConfirmButton: false,
          timer: 2000,
       });
          
        } catch (ex) {
          if (ex.response) { // When it is an response error
          //   this.$router.push('/login');
          const { data } = ex.response;
          const { errors, status } = data;
          commit('SET_ADD_VEHICLE', { 'message': status.message });
          this.$swal({
            title: status.message,
            icon:'error',
            showConfirmButton: false,
            timer: 2000,
         });
          
          if(status.message=="Validation Error"){
          commit('SET_ADD_VEHICLE', { 'message': status.message, 'type': 'error', 'errors': errors });
          console.log(data);
        }
          else{
            commit('SET_ADD_VEHICLE', { 'message': status.message, 'type': 'error', 'errors': errors });
            console.log(data);
          }
          }
        }
        },
        async editVehicle({ commit ,dispatch },vehicle) {
          try {
      
            const response =  await this.$axios.post('/vehicle/edit',vehicle);
            const { status } = response.data;
            
            console.log(response.data.data);
            
           commit('SET_EDIT_VEHICLE', response.data.data);
           dispatch('vehicles');
           this.$swal({
            title: status.message,
            icon:'success',
            showConfirmButton: false,
            timer: 2000,
         });
            
          } catch (ex) {
            if (ex.response) { // When it is an response error
            //   this.$router.push('/login');
            const { data } = ex.response;
            const { errors, status } = data;
            commit('SET_EDIT_VEHICLE', { 'message': status.message });
            this.$swal({
              title: status.message,
              icon:'error',
              showConfirmButton: false,
              timer: 2000,
           });
            
            if(status.message=="Validation Error"){
            commit('SET_EDIT_VEHICLE', { 'message': status.message, 'type': 'error', 'errors': errors });
            console.log(data);
          }
            else{
              commit('SET_EDIT_VEHICLE', { 'message': status.message, 'type': 'error', 'errors': errors });
              console.log(data);
            }
            }
          }
          },
       
    },
  
    mutations: {
      SET_VEHICLES(state, value) {
        state.vehicles = value;
      },
      SET_ADD_VEHICLE(state, value){
        state.addVehicle=value;
      },
      SET_EDIT_VEHICLE(state, value){
        state.editVehicle=value;
      }
    }
  }