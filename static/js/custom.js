//sidebar for desktop
$(".sideMenuBtn").click(function () {
  $("body").find(".dashboardSec").toggleClass("viewSideBar");
});

//sidebar for mobile
$(".sideMenuBtnH").click(function () {
  $("body").find(".dashboardSec").toggleClass("viewSideBar");
});

$(document).ready(function () {
  //tooltip
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle-second="tooltip"]').tooltip();
  $(".meeting").click(function () {
    $(".tooltip").tooltip("hide");
  });

  // submenu
  $(".subMenu, .subMenu1").hide();
  $(".hasNav > a").click(function () {
    if (false == $(this).find("ul").is(":visible")) {
      $(".hasNav").removeClass("selected");
    }
    $(this).parent("li").toggleClass("selected");
  });

  $(".hasSub").click(function () {
    $(".subMenu1").toggleClass("d-block");
  });

  // resize
  $(".resize").click(function () {
    $(this).parents(".mainHeading").next(".resizeBox").toggleClass("d-none");
  });

  // Footer init
  footerarea_css();

  // dropdown-menu
  $(".dropdown-menu").on("click", function (e) {
    e.stopPropagation();
  });
  $(".dropdown-menu .close").on("click", function () {
    $(this).parents().find(".show").removeClass("show");
    $(this).parents(".dropdown-menuMeeting").find(".listingMeetingCol").show();
    $(this).parents(".dropdown-menuMeeting").find(".addMeeting").show();
    $(this).parents(".dropdown-menuMeeting").find(".meetingForm").hide();
  });
  //add Meeting
  $(".addMeeting").click(function () {
    $(this).parents(".openTickets").find(".listingMeetingCol").hide();
    $(this).parents(".openTickets").find(".meetingForm").show();
    $(this).hide();
  });
});
//header shrink
$(function () {
  let shrinkHeader = 2;
  $(window).scroll(function () {
    let scroll = getCurrentScroll();
    if (scroll >= shrinkHeader) {
      $(".dashboardHeader").addClass("fixed-top");
    } else {
      $(".dashboardHeader").removeClass("fixed-top");
    }
  });

  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
  }
});

// Footer Fixed
function footerarea_css() {
  let window_height_for_footer = parseInt($(window).height());
  let document_height_for_footer = parseInt($("html body").outerHeight(true));
  if (document_height_for_footer < window_height_for_footer) {
    $(".footer").css({
      position: "fixed",
      bottom: "0",
      left: "0",
      right: "0",
    });
  } else {
    $(".footer").css({
      display: "block",
    });
  }
}
// custom function init
$(window).resize(function () {
  footerarea_css();
});


// use document.images and document.scripts and print the list of images and scripts on an html

//You have to create a variable which is string containing the text which is link you are intersted in.
let a = document.scripts;
let b = "text";   //any string
Array.from(a).forEach((el)=>{
      if((el.src).includes(b)) {
        console.log(el.src)
      }
                                  
})
//you have to fetch all the links from a given page which contain the text