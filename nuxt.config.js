export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'the360-admin-poc',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {rel:"stylesheet" ,href:"https://use.fontawesome.com/releases/v5.7.2/css/all.css"},
      {rel:'stylesheet' , href:"https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap"}
        ],
        script: [
          {
            src: '/js/plugins/jquery-3.2.1.slim.min.js'
         },
          {
            src: '/js/plugins/popper.min.js'
         },
          {
             src: '/js/plugins/bootstrap.min.js'
          },
          {
            src: '/js/plugins/vue-multiselect.min.js'
         },
        //   {
        //     src: '/js/custom.js'
        //  },

        ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/developer.css',
    '@/assets/css/responsive.css',
    '@/assets/css/style.css',
    '@/assets/css/plugins/bootstrap.min.css',
    '@/assets/css/plugins/vue-multiselect.min.css'
  ],
  

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    'bootstrap-vue/nuxt',
    'vue-sweetalert2/nuxt',
  ],
  
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL:'http://localhost:10010/api/v1'
  },
  //Nuxt JS auth configuration
  auth:{
    strategies:{
      local:{
        endpoints:{
          login:{url:'auth/login',method:'post',propertyName:'data.access_token'},
          logout: { url: 'user/logout', method: 'post' },
          user: { url: 'web-login/my-details', method: 'get',propertyName:'data.user' }
        },
        tokenRequired: true,
        tokenType: null
      }
    }
  },
  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
